﻿using HotelManagementLibrary.Models;
using System;
using System.Collections.Generic;

namespace HotelManagementLibrary.Data
{
    public interface IDatabaseData
    {
        void BookGuest(string firstName, string lastName, DateTime startDate, DateTime endDate, int roomTypeId);
        void CheckInGuest(int bookingId);
        List<RoomTypeModel> GetAvailableRoomTypes(DateTime startDate, DateTime endDate);
        public RoomTypeModel GetRoomTypeById(int Id);
        List<BookingFullModel> SearchBookings(string lastName);
    }
}