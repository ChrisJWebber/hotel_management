﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagementLibrary.Models
{
    public class RoomModel
    {
        public int Id { get; set; }
        public string RoomNumber { get; set; }
        public int RoomType { get; set; }

    }
}
