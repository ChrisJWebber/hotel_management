﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HotelManagementLibrary.Models
{
    public class RoomTypeModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Decimal PricePerNight { get; set; }

    }
}
